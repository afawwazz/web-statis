<?php
// Ahmad
// Pekan 3 - Hari 2 – Membuat Web Statis dengan Laravel

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome(Request $request) {
        $namadepan = $request['firstName'];
        $namabelakang = $request['lastName'];
        return view('welcome', compact('namadepan', 'namabelakang'));
    }
    //
}

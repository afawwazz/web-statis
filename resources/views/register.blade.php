<!-- Ahmad 
     Pekan 3 - Hari 2 – Membuat Web Statis dengan Laravel -->
<!DOCTYPE html>
<html>
	<head>
		<title>Register</title>
	</head>
	<body>
		<h1>Buat Account Baru!</h1>
		<h2>Sign Up Form</h2>
		<form action="/welcome" method='post'>
			@csrf
			<label for="first_name">First name:</label><br><br>
				<input type="text" name='firstName' id="first_name">
				<br><br>
			<label for="last_name">Last name:</label><br><br>
				<input type="text" name='lastName' id="last_name">
			<p>Gender:</p>
				<input type="radio" name="gender" id="male" value="Male">
					<label for="male">Male</label><br>
				<input type="radio" name="gender" id="female" value="Female">
					<label for="female">Female</label><br>
				<input type="radio" name="gender" id="other_gender" value="Other Gender">
					<label for="other_gender">Other</label>
			<p>Nationality:</p>
				<select name='nationality'>
					<option>Indonesia</option>
					<option>Amerika</option>
					<option>Inggris</option>
				</select>
			<p>Language Spoken:</p>
				<input type="checkbox" name="language" id="bahasa" value="Bahasa">
					<label for="bahasa">Bahasa Indonesia</label><br>
				<input type="checkbox" name="language" id="english" value="English">
					<label for="english">English</label><br>
				<input type="checkbox" name="language" id="other_language" value="Other Language">
					<label for="other_language">Other</label>
				<br><br>
			<label for="bio">Bio:</label><br><br>
				<textarea name='bio' cols="30" rows=10 id="bio"></textarea><br>
			<input type="submit" value="Sign Up">
		</form>
	</body>
</html>
